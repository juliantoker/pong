var app = require('http').createServer(handler)
	, io = require('socket.io').listen(app)
	, fs = require('fs');

var model = require(__dirname + '/model.js');
app.listen(8080);
var players = [];

function handler(req,res) {
	fs.readFile(__dirname + 'index.html',
		function(err, data) {
			if(err) {
				res.writeHead(500);
				return res.end('Error loading index.html')
			}

			res.writeHead(200);
			res.end(data);
		});
	
}

function testConnection(socket) {
	socket.emit('testthis', model.test());
}

function onClientConnection(client) {
	console.log('A new player has joined the game: ' + client.id);
	players.push(client);
}

function onClientDisconnect(client) {

}


io.on('connection', onClientConnection);
