(function(exports){

	"use strict";

var Game = function(id,players,start_time) {
	this.id = id;
	this.players = players;
	this.start_time = start_time;
	this.last_updated = start_time
};

Game.STARTING_SCORE = 0;
Game.MAX_PLAYERS = 2;
Game.PLAYER_PADDLE_YPOS = 100;
Game.OPPONENT_PADDLE_YPOS = 0;
Game.BALL_DEFAULT_POSITION = { 'x':0 , 'y':0};
Game.BALL_DEFAULT_VELOCITY = { 'x':0 , 'y':0};

var Player = function(player_id) {
	this.player_id = player_id;
	this.score = Game.STARTING_SCORE;
	this.yPos = Game.PLAYER_PADDLE_YPOS;
	this.score = Game.STARTING_SCORE;

}

var Ball = function(position, velocity){
	this.position = Game.BALL_DEFAULT_POSITION;
	this.velocity = Game.BALL_DEFAULT_VELOCITY;
}


//Updates the gamestate
function updateGame(players) {

}

function updatePaddlePosition(player) {

}

function updateBallPosition() {

}
function test() {
	return("you da man");
}


exports.test = test;
})((typeof process === 'undefined' || !process.versions)
   ? window.common = window.common || {}
   : exports);
